package com.example.api

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.aminography.choosephotohelper.ChoosePhotoHelper
import com.aminography.choosephotohelper.callback.ChoosePhotoCallback
import com.vmadalin.easypermissions.EasyPermissions
import com.example.MainActivity
import com.example.R

abstract class BaseFragment<T : BaseViewModel> : Fragment(){

    open lateinit var viewModel: T


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onStart() {
        super.onStart()
    }


    override fun onResume() {
        super.onResume()
    }

    companion object {
        const val TAG = "BaseFragment"
    }
}

val Fragment.navigationController
    get() = NavHostFragment.findNavController(this)